#!/usr/bin/env bash

set -e

tmpfile=".rules.tmp"

# Recursively find all rules except scaffold.yml files.
find c csharp go java javascript python scala -type f -name '*.yml' ! -name 'scaffold.yml' > "$tmpfile"

yajsv -s ci/rule_schema.yml -l="$tmpfile" -q
yajsv -s ci/rule_schema_custom.yml -l="$tmpfile" -q

rm "$tmpfile"

echo "All rules passed schema validation"
